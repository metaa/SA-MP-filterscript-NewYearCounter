> ⚠ Please note:  
> This README was retroactively created from the
> [original forum post](http://forum.sa-mp.com/showthread.php?t=297567)
> and was modified slightly to remove stuff like weird typos and expired
> links.

# <span style="color: #FF6600">NewYearCounter</span>
> v0.2a

Hello out there!  
Today I present to you **<span style="color: #FF6600">NewYearCounter
</span> v0.2a**, the second version of the little filterscript released
nearly a year ago!

## What is it?
It displays a little counter to the new year at the bottom of the
screen. Afterwards it displays "HAPPY NEW YEAR \[year]" for all of the
players that've been online during midnight or those who will join at
01/01.

All you have to do is to check
```c
#define German
```
so that you got the right display 

## Changelog
**[v0.2a]**:
- Cleaned up the code and remade the design (you can change it if you want to)

**[v0.2]**:
- Rerelease with a bit of better code

**[v0.1]**:
- First release
