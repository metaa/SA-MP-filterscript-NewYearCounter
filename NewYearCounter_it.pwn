/*####################################################################*\
##                                                                  ##
##  CNA (Contatore Nuovo Anno :D) v.0.2a [Tradotto da giovanni256]  ##
##                 Un lavoro per ItalianHack.org                    ##
##      Originale: NYC (New Year Counter :D) v.0.2a by Meta         ##
##                                                                  ##
\*####################################################################*/

#include <a_samp>

#pragma tabsize 0

//#define Italiano // Mostrare il dispaly con le opzioni in Italiano?

new cTime;
new Text:NYCounter;
new Text:HappyNewYearText;

public OnFilterScriptInit()
{
        NYCounter = TextDrawCreate(357.000000, 399.000000, "_");
        TextDrawAlignment(NYCounter, 2);
        TextDrawBackgroundColor(NYCounter, 255);
        TextDrawFont(NYCounter, 2);
        TextDrawLetterSize(NYCounter, 0.40000, 2);
        TextDrawColor(NYCounter, -16776961);
        TextDrawSetOutline(NYCounter, 1);
        TextDrawSetProportional(NYCounter, 1);

        new message[40], year, month, day;
        getdate(year, month, day);
        if(month != 1) { year--; }
        #if defined Italiano
                format(message, sizeof(message), "~>~ FELICE NUOVO ANNO ~<~~n~~y~%d!", year);
        #else
                format(message, sizeof(message), "~>~ HAPPY NEW YEAR ~<~~n~~y~%d!", year);
        #endif
        HappyNewYearText = TextDrawCreate(340.000000, 350.000000, message);
        TextDrawAlignment(HappyNewYearText, 2);
        TextDrawBackgroundColor(HappyNewYearText, 255);
        TextDrawFont(HappyNewYearText, 1);
        TextDrawLetterSize(HappyNewYearText, 1.000000, 4.000000);
        TextDrawColor(HappyNewYearText, 16777215);
        TextDrawSetOutline(HappyNewYearText, 1);
        TextDrawSetProportional(HappyNewYearText, 1);

    CounterTimer();
        cTime = SetTimer("CounterTimer", 400, 1);
        return 1;
}

public OnFilterScriptExit()
{
        TextDrawDestroy(NYCounter);
        TextDrawDestroy(HappyNewYearText);
        KillTimer(cTime);
}

public OnPlayerConnect(playerid)
{
    new year, month, day, hour, minute, second;
        getdate(year, month, day);
        gettime(hour, minute, second);
        if(day == 1 && month == 1 && (second > 0 || hour > 0))
        {
            TextDrawShowForPlayer(playerid, HappyNewYearText);
        }
}

forward CounterTimer();
public CounterTimer()
{
        new string[150];
        new year, month, day, hour, minute, second;
        getdate(year, month, day);
        gettime(hour, minute, second);
        if(month == 1 && day == 1)
        {
            TextDrawHideForAll(NYCounter);
            TextDrawShowForAll(HappyNewYearText);
            KillTimer(cTime);
        }
        else
        {
                gettime(hour, minute, second);

                new day2;
                switch(month)
                {
                    case 1, 3, 5, 7, 8, 10, 12: day2 = 31;
                    case 2: { if(year%4 == 0) { day2 = 29; } else { day2 = 28; } }
                    case 4, 6, 9, 11: day2 = 30;
                }
                month = 12 - month;
                day = day2 - day;
                hour = 24 - hour;               if(hour == 24) { hour = 0; }            if(minute != 0) { hour--; }
                minute = 60 - minute;   if(minute == 60) { minute = 0; }        if(second != 0) { minute--; }
                second = 60 - second;   if(second == 60) { second = 0; }

                #if defined Italiano
                format(string, sizeof(string), "~y~2012: ~g~%02d ~w~Me, ~g~%02d ~w~G, ~g~%02d ~w~H, ~g~%02d ~w~M, ~g~%02d ~w~S", month, day, hour, minute, second);
                #else
                format(string, sizeof(string), "~y~2012: ~g~%02d ~w~Mo, ~g~%02d ~w~D, ~g~%02d ~w~H, ~g~%02d ~w~M, ~g~%02d ~w~S", month, day, hour, minute, second);
                #endif
                TextDrawHideForAll(NYCounter);
                TextDrawSetString(NYCounter, string);
                TextDrawShowForAll(NYCounter);
        }
        return 1;
}

forward MMF_NewYearCounter(version[15]);
public MMF_NewYearCounter(version[15])
{
	if(strcmp(VERSION, version, true) && strlen(version))
	{
	    return 2;
	}
	return 1;
}
